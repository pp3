#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Standard build tool for python libraries.
"""

import os.path
from distutils.core import setup

from pp import version as VERSION


LONG_DESCRIPTION = """
PP3 is Python3x fork of:\
Parallel Python module (PP) provides an easy and efficient way to create \
parallel-enabled applications for SMP computers and clusters. PP module \
features cross-platform portability and dynamic load balancing. Thus \
application written with PP will parallelize efficiently even on \
heterogeneous and multi-platform clusters (including clusters running other \
application with variable CPU loads). Visit http://www.parallelpython.com \
for further information about PP-1.5.7 .
"""


setup(
        name="pp3",
        url="",
        version=VERSION,
        download_url="",
        author="",
        author_email="mimi.vx@gmail.com",
        py_modules=["ppworker", "pptransport", "ppauto", "pp"],
        scripts=["ppserver.py"],
        description="Parallel and distributed programming for Python",
        platforms=["Windows", "Linux", "Unix"],
        long_description=LONG_DESCRIPTION,
        license="BSD-like",
        classifiers=[
        "Topic :: Software Development",
        "Topic :: System :: Distributed Computing",
        "Programming Language :: Python",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Intended Audience :: Developers",
        "Development Status ::",
        ],
)
